import CardStory from './Card.story.svelte';
import markdown from './Card.md';

export default {
  title: 'Card',
  component: CardStory,
  parameters: {
    notes: {markdown},
  }
};

export const Card = () => ({
  Component: CardStory,
});
