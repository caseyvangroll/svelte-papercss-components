# [Article](https://www.getpapercss.com/docs/components/articles/)

## Props

None

## Usage

#### Simple
```jsx
<Article>
  <ArticleTitle>
    Article Title
  </ArticleTitle>
  <ArticleMeta>
    Written by Super User on 24 November 2017. Posted in Blog
  </ArticleMeta>
  <ArticleTextLead>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Pariatur repellendus excepturi, consequatur illo rerum, non sint asperiores dolore sapiente, vitae blanditiis.
    Officiis at quaerat modi earum, fugiat magni impedit, aperiam.
  </ArticleTextLead>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
  Corrupti iure totam nam debitis aliquid impedit, et quas omnis aspernatur optio molestias ex laborum quia.
  Ducimus culpa tempore, veritatis officia delectus dolore dignissimos reprehenderit vero,
  sunt odit placeat est non molestiae ipsa nam velit in iusto hic quasi similique facere. Maxime?
  
  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
  Corrupti iure totam nam debitis aliquid impedit, et quas omnis aspernatur optio molestias ex laborum quia.
  Ducimus culpa tempore, veritatis officia delectus dolore dignissimos reprehenderit vero,
  sunt odit placeat est non molestiae ipsa nam velit in iusto hic quasi similique facere. Maxime?
</Article>
```