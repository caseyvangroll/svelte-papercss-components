import AlertStory from './Alert.story.svelte';
import markdown from './Alert.md';

export default {
  title: 'Alert',
  component: AlertStory,
  parameters: {
    notes: {markdown},
  }
};

export const Simple = () => ({
  Component: AlertStory,
});

export const Dismissible = () => ({
  Component: AlertStory,
  props: {
    dismissible: true
  },
});
