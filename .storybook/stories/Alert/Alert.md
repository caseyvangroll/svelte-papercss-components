# [Alert](https://www.getpapercss.com/docs/components/alerts/)

## Props

| name        | type    | default | required       | allowed values                                       |
| ----------- | ------- | ------- | -------------- | ---------------------------------------------------- |
| id          | string  | -       | if dismissable |                                                      |
| color       | string  | primary |                | primary \| secondary \| success \| warning \| danger |
| dismissible | boolean | false   |                |                                                      |

## Usage

#### Simple
```jsx
<Alert color='success'>
  We did it!
</Alert>
```

#### Dismissible
```jsx
<Alert id='MyAlert' color='success' dismissible>
  We did it!
</Alert>
```