import ArticleStory from './Article.story.svelte';
import markdown from './Article.md';

export default {
  title: 'Article',
  component: ArticleStory,
  parameters: {
    notes: {markdown},
  }
};

export const Article = () => ({
  Component: ArticleStory,
});
