# [Badge](https://www.getpapercss.com/docs/components/badges/)

## Props

| name        | type    | default | required       | allowed values                                       |
| ----------- | ------- | ------- | -------------- | ---------------------------------------------------- |
| color       | string  | primary |                | primary \| secondary \| success \| warning \| danger |

## Usage

#### Simple
```jsx
<h1>
  Example h1 Heading
  <Badge color='primary'>
    Badge - Primary
  </Badge>
</h1>

<h2>
  Example h2 Heading
  <Badge color='secondary'>
    Badge - Secondary
  </Badge>
</h2>

<h3>
  Example h3 Heading
  <Badge color='success'>
    Badge - Success
  </Badge>
</h3>

<h4>
  Example h4 Heading
  <Badge color='warning'>
    Badge - Warning
  </Badge>
</h4>

<h5>
  Example h5 Heading
  <Badge color='danger'>
    Badge - Danger
  </Badge>
</h5>

<h6>
  Example h6 Heading
  <Badge color='primary'>
    Badge - Primary
  </Badge>
</h6>
```
