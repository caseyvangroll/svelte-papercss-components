import BadgeStory from './Badge.story.svelte';
import markdown from './Badge.md';

export default {
  title: 'Badge',
  component: BadgeStory,
  parameters: {
    notes: {markdown},
  }
};

export const Badge = () => ({
  Component: BadgeStory,
});
