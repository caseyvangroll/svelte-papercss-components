# [Button](https://www.getpapercss.com/docs/components/buttons/)

## Props

| name     | type             | default | required | allowed values                                       |
| -------- | ---------------- | ------- | -------- | ---------------------------------------------------- |
| size     | number \| string | -       |          | large \| small                                       |
| color    | string           | primary |          | primary \| secondary \| success \| warning \| danger |
| block    | boolean          | false   |          |                                                      |
| disabled | boolean          | false   |          |                                                      |

## Usage

```jsx
<Button size='large'>
  Large
</Button>
<Button>
  Default
</Button>
<Button size='small'>
  Small
</Button>
<Button disabled>
  Disabled
</Button>
<Button block>
  Block Level
</Button>
<Button color='success'>
  Confirm
</Button>
```