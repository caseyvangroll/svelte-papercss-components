import ButtonStory from './Button.story.svelte';
import markdown from './Button.md';

export default {
  title: 'Button',
  component: ButtonStory,
  parameters: {
    notes: {markdown},
  }
};

export const Button = () => ({
  Component: ButtonStory,
});
