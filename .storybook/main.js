module.exports = {
  stories: ['./**/*.stories.js'],
  addons: [
    '@storybook/addon-notes/register',
    '@storybook/addon-viewport/register'
  ],
};
